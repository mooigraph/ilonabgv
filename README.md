# ilonabgv

This is a bgv binary graph data parser in c with support for gzipped graph files  
This graph data format is used by the free and non-free compiler software from oracle  
This graph format is used by the closed-source compiler graph viewer igv from oracle  
This graph data format is more advanced then any other graph data format  
The specification of this graph data format is described in the seafoam project  
The graalvm software is GNU GPL Free software available at https://www.graalvm.org/  
  
~~~  
GraalVM is a universal virtual machine for running applications written in JavaScript,  
Python, Ruby, R, JVM-based languages like Java, Scala, Clojure, Kotlin, and LLVM-based  
languages such as C and C++.  
~~~  
  
To compile: 
./autogen.sh  
make  
make clean  
make cleaner 
  
There is more info about graalvm details avaible at  
semantics of graalvm IR at https://arxiv.org/abs/2107.01815  
seafoam tool for working with compiler graphs at https://github.com/Shopify/seafoam  
  
scan-build: No bugs found.  
gcc-11.1 -fanalyzer option: no comments  
  
This is a work-in-progress to be used with the gml4gtk graph viewer  
  
![screenshot](screenshot.png)
  
## links  
- [graalvm](https://www.graalvm.org)  
- [seafoam](https://github.com/Shopify/seafoam)  
- [ir-pdf](https://arxiv.org/abs/2107.01815)  
  
SPDX-License-Identifier: GPL-3.0+  
License-Filename: LICENSE  
  

