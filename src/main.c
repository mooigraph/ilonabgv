
/*
 *  Copyright 2021
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

#include "splay-tree.h"
#include "main.h"
#include "bgv.h"

char parsermessage[256];

int main(int argc, char *argv[])
{
	gzFile f = (gzFile) 0;
	int status = 0;
	if (argc) {
	}
	f = gzopen(argv[1], "r");
	if (f == (gzFile) 0) {
		f = gzopen("test.bgv", "r");
		/*      return (0); */
	}
	status = bgvparse(NULL, f, "fname", "argv0");
	printf("%s(): status=%d\n", __func__, status);
	gzclose(f);
	return (0);
}

/* end */
